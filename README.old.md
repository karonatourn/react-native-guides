# Codingate React Native Best Practise

## 1 Let's Start Javascript

### 1.1 Where to Learn Javascript 📚

- [MDN JavaScript Guide](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide)
- [You Don’t Know JavaScript](https://github.com/getify/You-Dont-Know-JS)
- [19+ JavaScript Shorthand Coding](https://www.sitepoint.com/shorthand-javascript-techniques/)

### 1.2 Javascript Style Guidelines 💡

- [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript/blob/master/README.md#objects)

### 1.3 Online Javascript Editor ⌨️

- [ES6 Console](https://es6console.com/)
- [JSbin](https://jsbin.com/?js,console)

### 1.4 Tips

- [19+ JavaScript Shorthand Coding](https://www.sitepoint.com/shorthand-javascript-techniques/)

## 2. Development Environment Setup

### 2.1 Visual Studio Code

- [Install](https://code.visualstudio.com/)

### 2.2 Visual Studio Extensions

#### 2.2.1 Coding Enhancement

- [Better Comments](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments)
- [Color Highlight](https://marketplace.visualstudio.com/items?itemName=naumovs.color-highlight) : This extension styles css/web colors found in your document.
- [ES7 React/Redux/GraphQL/React-Native snippets](https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets)
- [markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
- [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)

#### 2.2.2 Format

- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) : format your JavaScript / TypeScript / CSS

#### 2.2.3 Inspection

- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

#### 2.2.4 Color and Theme

- [vscode-icons](https://marketplace.visualstudio.com/items?itemName=robertohuertasm.vscode-icons)

#### 2.2.5 Git

- [Git History](https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory)

## 3 React Native

### 3.1 Learn the Basics

- [Install](https://facebook.github.io/react-native/docs/getting-started)
- [Learn th Basics](https://facebook.github.io/react-native/docs/tutorial)
- [Deeper with React](https://reactjs.org/docs/introducing-jsx.html)

### 3.2 Plugins

#### 3.2.1 UI

- [Lottie for React Native](https://github.com/react-native-community/lottie-react-native)
    - XCode ```Cycle in dependencies between targets 'LottieLibraryIOS' and 'LottieReactNative'```: Solution is to go to File > Workspace Settings > choose Legacy Build System
- [react-native-share](https://github.com/react-native-community/react-native-share)
- [react-native-off-canvas-menu](https://github.com/shoumma/react-native-off-canvas-menu)
- [react-native-image-picker](https://github.com/react-native-community/react-native-image-picker)
- [React Native Elements](https://react-native-training.github.io/react-native-elements/)
- [React Native Vector Icons](https://github.com/oblador/react-native-vector-icons)
- [React Native Snap Carousel](https://github.com/archriss/react-native-snap-carousel)
- [React Native Dialog](https://www.npmjs.com/package/react-native-dialog)
- [React Native Card View](https://www.npmjs.com/package/react-native-cardview)
- [React Native Camera](https://github.com/react-native-community/react-native-camera)
- [React Native Camera Kit](https://github.com/wix/react-native-camera-kit)
- [React Native Maps](https://github.com/react-community/react-native-maps)
- [React Navigation](https://reactnavigation.org/docs/en/getting-started.html)
- [NativeBase.io](https://docs.nativebase.io/)
- [react-native-animatable](https://github.com/oblador/react-native-animatable)
- [React Native Extended StyleSheet](https://github.com/vitalets/react-native-extended-stylesheet)
- [react-native-responsive-screen](https://github.com/marudy/react-native-responsive-screen)

#### 3.2.2 State

- [Redux](https://redux.js.org/) *(for more complex and scalable project)*
  - [React Redux](https://react-redux.js.org/)
  - [Redux Thunk](https://github.com/reduxjs/redux-thunk) *(good to get redux work with async action like: HTTP request)*
  - [Redux Saga](https://redux-saga.js.org/docs/introduction/BeginnerTutorial.html)
  - [Redux Logger](https://www.npmjs.com/package/redux-logger) *(inspect every state and action payload)*
  - [Redux Devtools](https://github.com/reduxjs/redux-devtools) *(beyond the redux logger)*
  - [Reselect](https://github.com/reduxjs/reselect) *(good for optimization plan with redux)*
  - [Useful redux middlewares](https://gitlab.com/karonatourn/react-native-guides/tree/master/redux/middlewares)
  - [Useful redux enhancers](https://gitlab.com/karonatourn/react-native-guides/tree/master/redux/enhancers)
- [Mobx](https://mobx.js.org/index.html) *(alternative to redux)*
  - [3 Reasons Why I Stopped Using setState](https://blog.cloudboost.io/3-reasons-why-i-stopped-using-react-setstate-ab73fc67a42e)
  - [Mobx State Tree](https://github.com/mobxjs/mobx-state-tree#examples)
  - [React Native with MobX — Getting Started](https://medium.com/react-native-training/react-native-with-mobx-getting-started-ba7e18d8ff44)
  - [Ditching setState for MobX](https://medium.com/react-native-training/ditching-setstate-for-mobx-766c165e4578)

#### 3.2.3 Database

- [React Native Sqlite 2](https://github.com/craftzdog/react-native-sqlite-2)
- [Realm](https://realm.io/docs/javascript/latest/)

#### 3.2.4 Localization 🏴󠁧󠁢󠁥󠁮󠁧󠁿

- [i18next](https://www.i18next.com/) + [React i18next](https://react.i18next.com/)

#### 3.2.5 Network 🖧

- [axios](https://github.com/axios/axios)
- [Firebase](https://rnfirebase.io/)

#### 3.2.6 Inspection 🔍

- [prop-types](https://www.npmjs.com/package/prop-types)

#### 3.2.7 Debugging 🐞

- [React Developer Tools](https://github.com/facebook/react-devtools)

#### 3.2.6 Miscellaneous

- [react-native-device-info](https://github.com/rebeccahughes/react-native-device-info)
- [Moment](https://momentjs.com/)
- [Lodash](https://lodash.com/)
- [react-number-format](https://www.npmjs.com/package/react-number-format)
- [Numeral.js](http://numeraljs.com/)
- [Awesome React Native](https://github.com/jondot/awesome-react-native)

### 3.3 Project Structures

```javascript
📁android
📁ios
📁src
    📁dummy
        📄index.js
    📁api
        📄index.js
    📁assets
        📁 fonts
            📄Roboto.ttf
            📄Khmer.ttf
        📁images
            🖼️ic_logo.png   // ic_ = icon
            🖼️bg_app.png    // bg_ = backgroun
        📁videos
            🎞️splash.mp
        📁sounds
            🎵click.mp3
    📁components
        📄Header.js
        📄DynamicList.js
    📁screens
        📁home
            📁components
                📄AnyComponent.js
            📄HomeScreen.js
        📁setting
            📁components
                📄AnyComponent.js
            📄SettingScreen.js
    📁translations
        📄en.json
        📄kh.json
        📄index.js
    📁routes
        📄index.js
    // BEGIN: Redux
    📁action
        📄actionTypes.js
        📄actions.js
        📄index.js
    📁reducer
        📄index.js
    📁middleware
        📄index.js
    📁saga
        📄index.js
    📁store
        📄index.js
    // END: Redux
    📁utils
        📄fonts.js
        📄util.js
        📄constant.js
        📄styles.js
        📄index.js
    📄App.js
📄index.js
```

#### Naming File

**``[Domain][Screen/Context][Component][Type]``**

Description

##### 1. Domain

> Which product owns this component?

The domain basically refer to the product scope.

Ex:

- Facebook
- ReactNative
- UnityCommunity
- etc...

##### 2. Screen or Context

> What is the parent component?
>
> Which product subpart/screen this component belongs ?

Ex:

- LoginScreen
- RegisterScreen
- DashboardScreen
- PromotionScreen
- ChatScreen
- SettingScreen
- AuthenticationScreen
- etc...

##### 3. Component

> What this component do ?

Ex:

- Switch
- Register
- etc...

##### 4. Type

Below, they are common types.

- Button
- Input
- Icon
- Form
- Switch
- Toggle
- Badge
- Avatar
- Header
- Slider
- List
- Card
- ListItem
- Screen
- etc...

#### Naming Folder

Use lower case and (-) to separate words

```javascript
app-store/
register-buttons/
```

#### Component Code

```javascript
// Package imports
import React, { Component } from "react";
...

export default class ComponentName extends Compnent {

    // Begin block of core functions
    constructor(props) {
        super(props);

        // Intiailize the component state here
        this.state = {
            ...
        };
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
    }

    ...
    // End block of core functions

    // Begin block of custom functions
    // Tell that it is used inside the class only
    _doAnything() {
    }
    
    // Tell that it can be used outside the class
    doSomething() {
    }
    ...
    // End block of custom functions
}

// Initialize default props
ComponentName.defaultProps = {
    ...
};

// Prop type inspection
ComponentName.propTypes = {
    ...
};

// Component styles
const styles = StyleSheet.create({
    ...
});
```

## 3.4 Troubleshooting and Tips 😎

### 3.4.1 Troubleshooting

- [Build: Could not install the app on the device, read the error above for details.](https://github.com/facebook/react-native/issues/8868)
- [setTimeout is broken](https://github.com/facebook/react-native/issues/9030)
- [Initial 0 aniamted value does not work for the first before animation starts](https://stackoverflow.com/questions/47278781/react-native-animation-not-working-properly-on-android)
- [Bundling React Native during Android release builds](https://proandroiddev.com/bundling-react-native-during-android-release-builds-ec52c24e200d)
- [Mobx: Can't find variable: Symbol](https://github.com/mobxjs/mobx/issues/1587)
- [Disable clearing badge in OneSignal in iOS with OneSignal_disable_badge_clearing=true](https://github.com/OneSignal/OneSignal-iOS-SDK/issues/335)

### 3.4.2 Tips

- [React Native Custom Font](https://medium.com/react-native-training/react-native-custom-fonts-ccc9aacf9e5e)
- [Add Custom Font](https://blog.bam.tech/developper-news/add-a-custom-font-to-your-react-native-app)
- [Animation Book](https://animationbook.codedaily.io)
- [6 Quick React Tips to Write A Better Code Pt.1](https://blog.alexdevero.com/quick-react-tips-pt1/)
- [Image suffixes @1x, @2x and @3x](https://developer.apple.com/design/human-interface-guidelines/ios/icons-and-images/image-size-and-resolution/)
- [Build responsive React Native views for any device and support orientation change](https://medium.com/react-native-training/build-responsive-react-native-views-for-any-device-and-support-orientation-change-1c8beba5bc23)
- [7 Tips to Develop React Native UIs For All Screen Sizes](https://medium.com/@shanerudolfworktive/7-tips-to-develop-react-native-uis-for-all-screen-sizes-7ec5271be25c)
- [41 Best React Native Libraries](https://rubygarage.org/blog/react-native-best-libraries)
- [Deep copy for object](https://medium.com/@tkssharma/objects-in-javascript-object-assign-deep-copy-64106c9aefab)

## 4. Helping Tools

- [Flat UI Colors](https://flatuicolors.com/) *(280 handpicked colors)*
- [App Icon Maker](http://appiconmaker.co/)
- [Diawi](https://www.diawi.com/) *(deploy Android/iOS application)*
- [JSONLint](https://jsonlint.com/)
- [JSONViewer](http://jsonviewer.stack.hu/)