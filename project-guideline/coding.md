[⬅️ Back](index.md)
# Coding Style Guide

- [JavaScript Coding Style](./js_coding_style.md)
- [TypeScript Coding Style](./ts_coding_style.md)
- [React Native Coding Style](./rn_coding_style.md)
- [Comment with JSDoc](https://github.com/google/closure-compiler/wiki/Annotating-JavaScript-for-the-Closure-Compiler)
- [JSON Coding Style](./json_coding_style.md)
- [Linting Tools](linting_tool.md)