# Dependency Guide
- **Choose better dependencies**
	- Choose the one with more stars reported in [github](https://github.com/ "github")
    
    ![alt react-native-map)](./images/img_star.png)
	- Choose the one with less issues and more close reported in [github](https://github.com/ "github")
    
    ![alt react-native-map)](./images/img_issue.png)
	- Choose the one with more downloads reported in [npm](https://www.npmjs.com/ "npm")
    
    ![alt download)](./images/img_download.png)
	- Choose the one with active improvement

    ![alt download)](./images/img_active.png)

    ![alt download)](./images/img_active1.png)
	- Choose the one with less internal dependencies

    ![alt react-native-map)](./images/img_map.png)
    - Choose the one with more collaborators and contributors

    ![alt react-native-map)](./images/img_collab.png)

    ![alt react-native-map)](./images/img_contrib.png)
- **Block version**
    - Install specific a version of dependency

        ```yarn add package-name@x.x.x```

    - Remove ```^``` from version

        ![alt react-native-map)](./images/img_block.png)

- **Make dependencies with Android native code to support Android X**
    
    [Use jetifier](https://www.npmjs.com/package/jetifier)

- **Less dependencies in better choice**
    
    Always try to have less dependencies as possible as you can in the project
