# Environment Setup Guide
- **Install React Native CLI**
	- [React Native](https://reactnative.dev/docs/getting-started)
- **Development IDE**
	- [Install Visual Studio Code](https://code.visualstudio.com/ "Install Visual Studio Code")
- **Visual Studio Code Extension**
	- **Code Format**
		- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
	- **Theme**
		- [vscode-icons](https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons)
		- [Material Icon Theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)
		- [Color Highlight](https://marketplace.visualstudio.com/items?itemName=naumovs.color-highlight)
	- **Snippet**
		- [React Redux ES6 Snippets](https://marketplace.visualstudio.com/items?itemName=timothymclane.react-redux-es6-snippets)
		- [ES7 React/Redux/GraphQL/React-Native snippets](https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets)
	- **Note**
		Installing more extensions for the IDE on a limited spec laptop can impact your laptop performance. Be careful when installing any of them.
- **Package Manager**
	- [Install Yarn](https://classic.yarnpkg.com/en/docs/install#mac-stable)