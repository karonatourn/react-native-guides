# Linting Tools

To produce quality coding, we have to think of 2 things:

- **Code Formatting**: ensure your code look beautiful in formatting style
- **Code Quality Checking**: ensure your code not having bugs

To achive above, we have to rely on the following pluggable tools:

- [**ESLint**](https://eslint.org/docs/user-guide/getting-started) : have rules for **Coding Formatting** and **Quality Checking**
- [**Prettier**](https://prettier.io/docs/en/index.html) : have better rules for **Coding Formatting** to override [**ESLint**](https://eslint.org/docs/user-guide/getting-started) rules
- [**Husky** 🐶](https://typicode.github.io/husky/#/) : help run above linting tools and more when you commit or push
- [**Lint Staged**](https://github.com/okonet/lint-staged) : help run above linting tools against staged git files and don't let 💩 slip into your code base!

**Note:**

In React Native, it is already shipped with **ESLint** and **Prettier** after initializing the project.

