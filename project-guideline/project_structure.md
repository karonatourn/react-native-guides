# Project Structure Guide

#### **Naming Project**

We use **PascalCase** for the project name

```bash
// bad
npx react-native init project-name

// good
npx react-native init ProjectName
```

#### **Folder Structure**

```javascript
📁android
📁ios
📁src
    📁dummy
        📄index.js
    📁api
        📄index.js
    📁assets
        📁 fonts
            📄Roboto.ttf
            📄Khmer.ttf
        📁images
            🖼️ic_logo.png   // ic_ = icon
            🖼️bg_app.png    // bg_ = backgroun
        📁videos
            🎞️splash.mp4
        📁sounds
            🎵click.mp3
    📁components
        📄Header.js
        📄DynamicList.js
    📁screens
        📁home
            📁components
                📄AnyComponent.js
            📄HomeScreen.js
        📁setting
            📁components
                📄AnyComponent.js
            📄SettingScreen.js
    📁translations
        📄en.json
        📄kh.json
        📄index.js
    📁routes
        📄index.js
    // BEGIN: Redux
    📁action
        📄actionTypes.js
        📄actions.js
        📄index.js
    📁reducer
        📄index.js
    📁middleware
        📄index.js
    📁saga
        📄index.js
    📁store
        📄index.js
    // END: Redux
    📁utils
        📄fonts.js
        📄util.js
        📄constant.js
        📄styles.js
        📄index.js
    📄App.js
📄index.js
```

#### **Naming File**

**``[Domain][Screen/Context][Component][Type]``**

**Description**

##### **1. Domain**

> Which product owns this component?

The domain basically refer to the product scope.

Ex:

- Facebook
- ReactNative
- UnityCommunity
- etc...

##### **2. Screen or Context**

> What is the parent component?
>
> Which product subpart/screen this component belongs ?

Ex:

- LoginScreen
- RegisterScreen
- DashboardScreen
- PromotionScreen
- ChatScreen
- SettingScreen
- AuthenticationScreen
- etc...

##### **3. Component**

> What this component do ?

Ex:

- Switch
- Register
- etc...

##### **4. Type**

Below, they are common types.

- Button
- Input
- Icon
- Form
- Switch
- Toggle
- Badge
- Avatar
- Header
- Slider
- List
- Card
- ListItem
- Screen
- etc...

#### **Naming Folder**

Use [**kabab-case**](https://en.wikipedia.org/wiki/Letter_case#Special_case_styles) to name a folder

```javascript
app-store/
register-buttons/
```