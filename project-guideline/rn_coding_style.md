# React Native Coding Style

- Use JavaScript ES6

    https://developer.mozilla.org/en-US/docs/Archive/Web/JavaScript/New_in_JavaScript/ECMAScript_2015_support_in_Mozilla

    http://es6-features.org

    https://www.javascripttutorial.net/es6/

    https://www.w3schools.com/js/js_es6.asp

- 1 Tab must be 2 spaces

- Component

    ```javascript
    // ComponentName.js

    import React, { Component } from "react";
    ...

    export default class ComponentName extends Compnent {

        // Begin block of core functions
        constructor(props) {
            super(props);

            // Intiailize the component state here
            this.state = {
                ...
            };

            this._anyVariable = anyValue;
        }

        componentDidMount() {
        }

        componentWillUnmount() {
        }

        render() {
        }

        ...
        // End block of core functions

        // Begin block of custom functions
        // Tell that it is used inside the class only
        _doAnything() {
        }
        
        // Tell that it can be used outside the class
        doSomething() {
        }
        ...
        // End block of custom functions
    }

    // Initialize default props
    ComponentName.defaultProps = {
        ...
    };

    // Prop type inspection
    ComponentName.propTypes = {
        ...
    };

    // Component styles
    const styles = StyleSheet.create({
        ...
    });
    ```
- Utility Scripts
    ```javascript
    // appUtil.js

    export function doSomething (...) {
        // Solution here
    }
    ```

- [Hook](https://reactjs.org/docs/hooks-reference.html) can be useful in function Component