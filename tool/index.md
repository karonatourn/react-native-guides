# Supporting Tools

**Design**
- [Flat UI Colors](https://flatuicolors.com/) *(280 handpicked colors)*
- [Colorhunt](https://colorhunt.co)
- [App Icon Maker](http://appiconmaker.co/)
- [Image compression with Tinypng](https://convertio.co/mp3-caf/)
- [Remove Image Background](https://www.remove.bg)
- [React Native Vector Icons](https://github.com/oblador/react-native-vector-icons)
- [Icon in Fonts with IcoMoon](https://icomoon.io)
- [Animated Files with LottieFiles](https://lottiefiles.com)
- [Flat Icon](https://www.flaticon.com)
- [SVG to PNG](https://svgtopng.com)

**Coding**
- [JSONLint](https://jsonlint.com/)
- [JSONViewer](http://jsonviewer.stack.hu/)
- [ES6 Console](https://es6console.com/)
- [JSbin](https://jsbin.com/?js,console)
- [Snack Expo](https://snack.expo.io)
- [SVG to React Native Component](https://react-svgr.com/playground/)

**Deployment Distribution**
- [Diawi](https://www.diawi.com/)
- [App Center](https://appcenter.ms/apps)

**Misc**
- [Audio Converter with Convertio](https://convertio.co/mp3-caf/)
- [Audio Joiner](https://audio-joiner.com)