# Troubleshooting and Tips

### Troubleshooting

- [Build: Could not install the app on the device, read the error above for details.](https://github.com/facebook/react-native/issues/8868)
- [setTimeout is broken](https://github.com/facebook/react-native/issues/9030)
- [Initial 0 aniamted value does not work for the first before animation starts](https://stackoverflow.com/questions/47278781/react-native-animation-not-working-properly-on-android)
- [Bundling React Native during Android release builds](https://proandroiddev.com/bundling-react-native-during-android-release-builds-ec52c24e200d)
- [Mobx: Can't find variable: Symbol](https://github.com/mobxjs/mobx/issues/1587)
- [Disable clearing badge in OneSignal in iOS with OneSignal_disable_badge_clearing=true](https://github.com/OneSignal/OneSignal-iOS-SDK/issues/335)

### Tips

- [React Native Custom Font](https://medium.com/react-native-training/react-native-custom-fonts-ccc9aacf9e5e)
- [Add Custom Font](https://blog.bam.tech/developper-news/add-a-custom-font-to-your-react-native-app)
- [Animation Book](https://animationbook.codedaily.io)
- [6 Quick React Tips to Write A Better Code Pt.1](https://blog.alexdevero.com/quick-react-tips-pt1/)
- [Image suffixes @1x, @2x and @3x](https://developer.apple.com/design/human-interface-guidelines/ios/icons-and-images/image-size-and-resolution/)
- [Build responsive React Native views for any device and support orientation change](https://medium.com/react-native-training/build-responsive-react-native-views-for-any-device-and-support-orientation-change-1c8beba5bc23)
- [7 Tips to Develop React Native UIs For All Screen Sizes](https://medium.com/@shanerudolfworktive/7-tips-to-develop-react-native-uis-for-all-screen-sizes-7ec5271be25c)
- [41 Best React Native Libraries](https://rubygarage.org/blog/react-native-best-libraries)
- [Deep copy for object](https://medium.com/@tkssharma/objects-in-javascript-object-assign-deep-copy-64106c9aefab)